// Function to Gather the details of the ships.
//Gets ship details entered by user to input boxes to be saved to local storage
function createNewShip()
	{
		shipNameRef = document.getElementById("shipName").value;				// Gets the name of the Ship to the HTML file
		maxSpeedRef = Number(document.getElementById("maxSpeed").value);    	// gets the Max speed of the Ship to HTML file
		rangeRef = Number(document.getElementById("range").value);				// Shows the range of the Ship to the HTML file
		shipDescriptionRef = document.getElementById("shipDescription").value;	// Shows the details of the Ship to the HTML file
		costRef = Number(document.getElementById("cost").value);
		let shipKey = "shiplist";
		console.log(shipNameRef);
		
		var shipListInstance = new ShipList();									// Gives the list 
		shipListInstance.addShip();
		if (typeof(Storage) !== "undefined")
    {
        //Stringify deckInstance to a JSON string
		var jsonshiplist = JSON.stringify(localShipList);
		
        //store this JSON string to local storage using the key 
		localStorage.setItem(shipKey,jsonshiplist);
		
		window.alert("Route added successfully");
		window.location.href = "index.html";

    }
    else
    {
        window.alert("Error: localStorage is not supported by current browser.");
    }	
	};