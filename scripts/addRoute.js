mapboxgl.accessToken = 'pk.eyJ1IjoidmluZXRoIiwiYSI6ImNrZTVrODRmdjEzdHUzMm43eG00czRlZWgifQ.gK2Agmj8cAGRYVHEGab6VA';

	
	var shipDropdown = document.getElementById("shipName");
	
	var destPortDropdown = document.getElementById("destinationPort");
	var sourcePortDropdown = document.getElementById("sourcePort");
		
	

jsonpRequest("https://eng1003.monash/api/v1/ports/?callback=portsResponse");

function jsonpRequest(url)
	{
		let script = document.createElement('script');
		script.src = url;
		document.body.appendChild(script);
		
	}


	
//Adds drop down options for all api Ports		
function portsResponse(portArray)
	{
		//source Ports
		for(var i = 0; i<portArray.ports.length; i++) 
		{
			var option = document.createElement("option");
			option.value = JSON.stringify(portArray.ports[i]);
			option.text = portArray.ports[i].name + ", " + portArray.ports[i].country + "(API)";
			sourcePortDropdown.add(option)
		}
		//Destination Ports
		for(var i = 0; i<portArray.ports.length; i++) 
		{
			var option = document.createElement("option");
			option.value = JSON.stringify(portArray.ports[i]);
			option.text = portArray.ports[i].name + ", " + portArray.ports[i].country + "(API)";
			destPortDropdown.add(option)
		}
		
	}
//Adds drop down options for all local Ports		
	for(var i = 0; i<localPortList.length; i++) 
	{
		//source Ports
		var option = document.createElement("option");
		option.value = JSON.stringify(localPortList[i]);
		option.text = localPortList[i].name + "(User)";
		sourcePortDropdown.add(option);
	}
		//Destination Ports
		for(var i = 0; i<localPortList.length; i++) 
	{
		var option = document.createElement("option");
		option.value = JSON.stringify(localPortList[i]);
		option.text = localPortList[i].name + "(User)";
		destPortDropdown.add(option);
	}
	var sourceCoordinates = [];
	var destCoordinates = [];
	var centreCoordinates = [];
	var map;
	var points;
	var selectedSourcePort;
	var selectedDestPort;
//Adds markers to selected source port and destination port when add route button is pushed	
function addRoute()
{
	points = [];
	selectedSourcePort = JSON.parse(sourcePortDropdown.value);
	console.log(selectedSourcePort);
	
	selectedDestPort = JSON.parse(destPortDropdown.value);
	console.log(selectedDestPort);
	
		sourceCoordinates = [selectedSourcePort["lng"],selectedSourcePort["lat"]];
		console.log(sourceCoordinates)
		
		destCoordinates = [selectedDestPort["lng"],selectedDestPort["lat"]];
		console.log(destCoordinates)
		
		var centre = turf.midpoint(turf.point(sourceCoordinates),turf.point(destCoordinates));
		centreCoordinates = [centre.geometry.coordinates[0],centre.geometry.coordinates[1]];
		console.log(centre)
		console.log(centreCoordinates)
			 map = new mapboxgl.Map({
				container: 'map',
				style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
				center:[centre.geometry.coordinates[0],centre.geometry.coordinates[1]] ,// centre of source and destination  position [lat, lng]
				zoom: 1 // starting zoom
			});
			
			
			//Make green marker at source
				var marker = new mapboxgl.Marker({color: 'green'})
			.setLngLat(sourceCoordinates)
			.setPopup(new mapboxgl.Popup().setHTML(selectedSourcePort["country"] + ", " + selectedSourcePort["name"]))
			.addTo(map);
                marker.togglePopup();
				
			//Make green marker at destination
				var marker = new mapboxgl.Marker({color: 'red'})
			.setLngLat(destCoordinates)
			.setPopup(new mapboxgl.Popup().setHTML(selectedDestPort["country"] + ", " + selectedDestPort["name"]))
			.addTo(map);
             marker.togglePopup();
            
			
			// Function to add waypoint is called when map is clicked on
			//e contains coordinates of click which is passed to addroute
			map.on('click', function(e){
			console.log(e)
			addWaypoint(e.lngLat)
			});
			//push source port coordinates to as first element of points array
			points.splice(0, 1, sourceCoordinates);
			console.log(points);
			//push source port coordinates to as first element of points array
			points.splice(1, 1, destCoordinates);
			console.log(points);
		
			document.getElementById("getShipsId").disabled = false;
			waypointCounter = 0;
			//creates line between markers when map loads
			map.on('load',function (e){
			showRoute()
			});
	


}
	var waypointCounter= 0;
	var waypointMarker = [];
	

	//creates waypoint when clicked on the map		
	function addWaypoint(coordinates)
	{
		waypointCounter++;
		console.log(coordinates)
		 waypointMarker[waypointCounter] = new mapboxgl.Marker({
			draggable: true,
			color: 'orange'
		})
			.setLngLat(coordinates)
			.setPopup(new mapboxgl.Popup().setHTML("Waypoint " + waypointCounter ))
			.addTo(map);
			waypointMarker[waypointCounter].togglePopup();
			
			document.getElementById("setwaypopint").disabled = false;
			document.getElementById("removeWaypointId").disabled = false;
			console.log(waypointMarker)
			return waypointCounter;
	}

	
	var waypointCoordinates = [];
	var count=0;
	var addedWaypoints = [];
	var tempWaypoints = [];
	
	//pushes waypoint to points array and creates new route incorparating waypoint
		function setWaypoint() 
			{
				if (waypointCounter>0 && waypointCounter<2 )
						
				{
					waypointCoordinates[count] = waypointMarker[waypointCounter]._lngLat;
					tempWaypoints.push([waypointCoordinates[count].lng,waypointCoordinates[count].lat]);
					addedWaypoints.push([waypointCoordinates[count].lng,waypointCoordinates[count].lat]);
					console.log(waypointCoordinates)
					console.log(tempWaypoints)
					
					waypointCoordinates[count] = waypointMarker[waypointCounter]._lngLat;
					console.log(waypointCoordinates)
					waypointMarker[waypointCounter].setDraggable();
					points.splice(-1, 0, [waypointCoordinates[count].lng,waypointCoordinates[count].lat]);
					count++;
					console.log()
					document.getElementById("setwaypopint").disabled = true;
					
					document.getElementById("removeWaypointId").disabled = true;
					showRoute();
				}
				else if(waypointCounter>=2)
				{
						waypointCoordinates[count] = waypointMarker[waypointCounter]._lngLat;
						tempWaypoints.push([waypointCoordinates[count].lng,waypointCoordinates[count].lat]);
						if(tempWaypoints.length>2)
						{
							tempWaypoints.shift();
						};
						console.log(tempWaypoints)
						let markerLine = turf.lineString(tempWaypoints);
						var markerDistance = turf.length(markerLine, {units: 'kilometers'});
						console.log(markerDistance)
					
					
						if(markerDistance>=100)
							
						{
							addedWaypoints.push([waypointCoordinates[count].lng,waypointCoordinates[count].lat]);
							waypointCoordinates[count] = waypointMarker[waypointCounter]._lngLat;
							console.log(waypointCoordinates)
							waypointMarker[waypointCounter].setDraggable();
							points.splice(-1, 0, [waypointCoordinates[count].lng,waypointCoordinates[count].lat]);
							count++;
							console.log()
							document.getElementById("setwaypopint").disabled = true;
							document.getElementById("removeWaypointId").disabled = true;
							showRoute();
							calculaterouteDistance();
						}
					else
						{
							window.alert("Waypoint too close to previous waypoint");
							
						}
				}
				
            
			};

		function removeWaypoint()
		{
			waypointMarker[waypointCounter].remove();
			waypointCounter--;
			document.getElementById("setwaypopint").disabled = true;
			document.getElementById("removeWaypointId").disabled = true;

		};
		
var routeDistance;
// Gets ships from api and is added to a dropdown menu
function getShips()
{
	showRoute();
	calculaterouteDistance();
	jsonpRequest("https://eng1003.monash/api/v1/ships/?callback=shipsResponse");
				
		
		for(var i = 0; i<localShipList.length; i++) 
		{
			if(localShipList[i].range>=routeDistance)
			{
				
				var option = document.createElement("option");
				option.value = JSON.stringify(localShipList[i]);
				option.text = localShipList[i].name + "(User)";
				shipDropdown.add(option);
			}
			
		}
			
}

function shipsResponse(shipArray)
{
		
		
		for(var i = 0; i<shipArray.ships.length; i++) 
		{
		if(shipArray.ships[i].range>=routeDistance && shipArray.ships[i].status == "available")
			{
				let option = document.createElement("option");
				option.value = JSON.stringify(shipArray.ships[i]);
				option.text = shipArray.ships[i].name + "(API)";
				shipDropdown.add(option);
				calculateCost();
				calculateTime();
			}
		}
		
		
}	
//makes json line between points
function showRoute()
{
	// Code added here will run when the "Show Path" button is clicked.
				removeLayerWithId('route');
				map.addSource('route', {
				'type': 'geojson',
				'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': points
                }
            }
        });
        map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#888',
                'line-width': 5
            }
        })
}
        // This function checks whether there is a map layer with id matching 
        // idToRemove.  If there is, it is removed.
        function removeLayerWithId(idToRemove)
        {
            let hasPoly = map.getLayer(idToRemove)
            if (hasPoly !== undefined)
            {
                map.removeLayer(idToRemove)
                map.removeSource(idToRemove)
            }
        };
		

//calculates the distance using turf.js api 	
	function calculaterouteDistance()

	{
		var line = turf.lineString(points);
		routeDistance = turf.length(line, {units: 'kilometers'});
		document.getElementById("distanceOutput").innerHTML = (Number(routeDistance).toFixed(2) + " Km");
		console.log(routeDistance)
	}
	var routeCost;
	var speedInKm;
	var estimatedTime;
	var routeNameRef;
	var shipNameRef;
	var startDateRef;
	var sourcePortName;
	var destPortName;
	
	function calculateTime()
	{
		const knotsToKm = 1.852;
		let shipRef = JSON.parse(shipDropdown.value);
		speedInKm = shipRef.maxSpeed * knotsToKm;
		estimatedTime = (routeDistance/speedInKm);
		document.getElementById("estimatedTimeOutput").innerHTML = (Number(estimatedTime).toFixed(2) + " Hours");
		console.log(speedInKm);
	}
	
	function calculateCost()
	{
		let shipRef = JSON.parse(shipDropdown.value);
		routeCost = (shipRef.cost * speedInKm);
		document.getElementById("costOutput").innerHTML = ( "$" + Number(routeCost).toFixed(2));
		console.log(shipRef);
	}
	
	function removeRoute()
	{
		window.location.href = "addRoute.html";
	}
	//passes variables to addRoute in shared js and new route list is saved to local storage
	function saveRoute()
	{
		routeNameRef =  document.getElementById("routeName").value
		let shipRef = JSON.parse(shipDropdown.value);
		sourcePortName=([selectedSourcePort.country,selectedSourcePort.name]);
		destPortName=([selectedDestPort.country,selectedDestPort.name]);
		shipNameRef = shipRef.name;
		startDateRef = document.getElementById("startDate").value;
		
		let routeKey = "routeList"
		
		console.log(startDateRef);
		
		var routeInstance = new RouteList();
		routeInstance.addRoute();
		
		//sorts new route to local array before saving
		if (localRouteList.length>=1)
		{
			for(var j =0; (j+1)<localRouteList.length; j++)
			{
				console.log(localRouteList);
				console.log(j);
				localRouteList.sort(function() 
				{	
				console.log(localRouteList.length);
				var dateA = new Date(localRouteList[j]._startDate), dateB = new Date(localRouteList[j+1]._startDate);
				return dateB - dateA;
				});
				console.log(localRouteList);
			};
		};
	//checks if storage is available before saving
	if (typeof(Storage) !== "undefined")
    {
        
		//Stringify  to a JSON string
		let jsonportlist = JSON.stringify(localRouteList);
		
        //store this JSON string to local storage using the key 
		localStorage.setItem(routeKey,jsonportlist);
		
		window.alert("Route added successfully");
		window.location.href = "index.html";

    }
    else
    {
        window.alert("Error: localStorage is not supported by current browser.");
    }
};
