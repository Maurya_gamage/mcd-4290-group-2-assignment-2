var viewPortsTableRef = document.getElementById('viewPortsTable');

var data = {
 callback: "portsResponse"
};

jsonpRequest("https://eng1003.monash/api/v1/ports/", data);
function jsonpRequest(url, data)
{
	// Build URL parameters from data object.
	 var params = "";
	 // For each key in data object...
	 for (let key in data)
	 {
		 if (data.hasOwnProperty(key))
		 {
			if (params.length == 0)
		 {
			// First parameter starts with '?'
			params += "?";
		 }
			else
		 {
			 // Subsequent parameter separated by '&'
			 params += "&";
		 }
		 var encodedKey = encodeURIComponent(key);
		 var encodedValue = encodeURIComponent(data[key]);
		 params += encodedKey + "=" +encodedValue;

		 }
	 }
	 var script = document.createElement('script');
	 script.src = url + params;
	 document.body.appendChild(script);
}
//
function portsResponse(portArray)
{
    var listHTML = "";
		// The loop below obtains the details of the port from ports API and display them in the HTML 
		for (var i=0; i<portArray.ports.length; i++)
			{
				var PortName = portArray.ports[i].name;
				var PortCountry = portArray.ports[i].country;
				var PortType = portArray.ports[i].type;
				var PortSize = portArray.ports[i].size;
				var PortLocprecision = portArray.ports[i].locprecision;
				var PortLat = Number(portArray.ports[i].lat).toFixed(2);
				var PortLng = Number(portArray.ports[i].lng).toFixed(2);
			
			
    // And sample JavaScript code that would generate the HTML above is:
    //
	listHTML += "<tr> <td class=\"full-width mdl-data-table__cell--non-numeric\">" + PortName + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + PortCountry + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + PortType + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + PortSize + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + PortLocprecision + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + PortLat + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + PortLng + "</td></tr>";
			};

    // Insert the list view elements into the flights list.
   viewPortsTableRef.innerHTML += listHTML;
}


 var listHTML = "";
console.log(localPortList.length);
		
		// The loop below obtains the details of the port from ports User and display them in the HTML 
		for (let i=0; i<localPortList.length; i++)
			{
				var localPortName = localPortList[i].name;
				var localPortCountry = localPortList[i].country;
				var localPortType = localPortList[i].type;
				var localPortSize = localPortList[i].size;
				var localPortLocprecision = "unknown";
				var localPortLat = Number(localPortList[i].lat).toFixed(2);
				var localPortLng =  Number(localPortList[i].lng).toFixed(2);
			console.log(localPortName);
			
    // And sample JavaScript code that would generate the HTML above is:
    //
	listHTML += "<tr> <td class=\"full-width mdl-data-table__cell--non-numeric\">" + localPortName + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + localPortCountry + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + localPortType + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + localPortSize + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + localPortLocprecision + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + localPortLat + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + localPortLng + "</td></tr>";
			};

    // Insert the list view elements into the flights list.
   viewPortsTableRef.innerHTML += listHTML;