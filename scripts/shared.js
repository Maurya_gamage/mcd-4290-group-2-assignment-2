// Feature 1

class Ship

{
    constructor(name, maxSpeed, range, description, cost, shipStatus)
    {
        this.name = name;							
		this.maxSpeed = maxSpeed;					
        this.range = range;						
        this.desc = description;			 
        this.cost = cost;							
		this.status = shipStatus;					
    }

     getname()
    {
        return this.name;
    }
    setname(newName)
    {
        this.name = newName;
    }

    getmaxSpeed()
    {
        return this.maxSpeed;
    }
    setmaxSpeed(newMaxSpeed)
    {
        this.maxSpeed = newMaxSpeed;
    }

    getrange()
    {
        return this.range;
    }
    setrange(newRange)
    {
        this.range = newRange;
    }
	
	getdescription()
    {
        return this.desc;
    }
    setdescription(newDescription)
    {
        this.desc = newDescription;
    }
	
	getcost()
    {
        return this.cost;
    }
    setcost(newCost)
    {
        this.cost = newCost;
    }
	
	getshipStatus()
    {
        return this.status;
    }
    setshipStatus(newStatus)
    {
        this.status = newStatus;
    }
}


class Port
{
    constructor(name, country, type, size, latitude, longitude)
    {
        this.name = name;
        this.country = country;
        this.type = type;
        this.size = size;
        this.lat = latitude;
		this.lng = longitude;
    }

    getname()
    {
        return this.name;
    }
    setname(newName)
    {
        this.name = newName;
    }

    getcountry()
    {
        return this.country;
    }
    setcountry(newCountry)
    {
        this.country = newCountry;
    }

    gettype()
    {
        return this.type;
    }
    settype(newType)
    {
        this.type = newType;
    }
	
	getsize()
    {
        return this.size;
    }
    setsize(newSize)
    {
        this.size = newSize;
    }
	
	getlatitude()
    {
        return this.lat;
    }
    setlatitude(newLatitude)
    {
        this.lat = newLatitude;
    }
	
	getlongitude()
    {
        return this.lng;
    }
    setlongitude(newLongitude)
    {
        this.lng = newLongitude;
    }
}

class Route
{
    constructor(name, ship, sourcePort, destinationPort, distance, time, cost, startDate, wayPointList)
    {
        this._name = name;
        this._ship = ship;
        this._sourcePort = sourcePort;
        this._destinationPort = destinationPort;
        this._distance = distance;
		this._time = time;
		this._cost = cost;
		this._startDate = startDate;
		this._wayPointList = wayPointList;
    }

    getname()
    {
        return this._name;
    }
    setname(newName)
    {
        this._name = newName;
    }

    getship()
    {
        return this._ship;
    }
    setship(newShip)
    {
        this._ship = newShip;
    }
	
    getsourcePort()
    {
        return this._sourcePort;
    }
    setsourcePort(newSourcePort)
    {
        this._sourcePort = newSourcePort;
    }

    getdestinationPort()
    {
        return this._destinationPort;
    }
    setdestinationPort(newDestinationPort)
    {
        this._destinationPort = newDestinationPort;
    }
	
	getdistance()
    {
        return this._distance;
    }
    setdistance(newDistance)
    {
        this._distance = newDistance;
    }
	
	gettime()
    {
        return this.time;
    }
    settime(newTime)
    {
        this._time = newTime;
    }
	
	getcost()
    {
        return this._cost;
    }
    setcost(newCost)
    {
        this._cost = newCost;
    }
	
	getstartDate()
    {
        return this._startDate;
    }
    setstartDate(newStartDate)
    {
        this._startDate = newStartDate;
    }
	
	getwayPointList()
    {
        return this._wayPointList;
    }
    setwayPointList(newWayPointList)
    {
        this._wayPointList = newWayPointList;
    }
}

var localShipList =JSON.parse(localStorage.getItem("shiplist"));

if (localShipList==null)
{
	localShipList=[];
}


class ShipList
{
    constructor(ship)
    {
        this._shipList = [];
    }

	getship()
    {
        return this._shipList;
    }
    setship(newShip)
    {
        this._shipList = newShipList;
    }

	addShip()
	{
		localShipList.push(new Ship(shipNameRef,maxSpeedRef,rangeRef,shipDescriptionRef,costRef,"available"))
		
	}
	
} 
var localPortList =JSON.parse(localStorage.getItem("portlist"));

if (localPortList==null)
{
	localPortList=[];
}


class PortList
{
    constructor(port)
    {
        this._port = [];
    }
	
	getport()
    {
        return this._port;
    }
    setport(newPort)
    {
        this._port = newPort;
    }
	
	addPort()
	{
		localPortList.push(new Port(portNameRef,countryRef,typeRef,sizeRef,latRef,lngRef))
	}
	
} 
var localRouteList =JSON.parse(localStorage.getItem("routeList"));

if (localRouteList==null)
{
	localRouteList=[];
}


class RouteList
{
    constructor(route)
    {
        this._route = [];
    }
	
	getroute()
    {
        return this._route;
    }
    setroute(newRoute)
    {
        this._route = newRoute;
    }
	
	addRoute()
	{
	localRouteList.push(new Route(routeNameRef, shipNameRef, [selectedSourcePort], [selectedDestPort], routeDistance, estimatedTime, routeCost, startDateRef, addedWaypoints))
		
	}
}


 




 