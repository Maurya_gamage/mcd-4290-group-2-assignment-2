var viewRouteTableRef = document.getElementById('viewRouteTable');
var routeList = [];

var listHTML = "";


if(localRouteList.length>0)
	{
		//Displays local routes to the main page
	for (var k=0; k<localRouteList.length; k++)
		{
		
				var localRouteName = localRouteList[k]._name;
				console.log();
				var localRouteSourcePort = localRouteList[k]._sourcePort[0].country;
				var localRouteDestinationPort = localRouteList[k]._destinationPort[0].country;
				
				var localRouteDistance = Number(localRouteList[k]._distance).toFixed(2);
				var localRouteStartDate = localRouteList[k]._startDate;
				var localRouteShip = localRouteList[k]._ship;
				var localRouteCost = Number(localRouteList[k]._cost).toFixed(2);
				var localRouteTime = Number(localRouteList[k]._time).toFixed(2);
				var localRouteWayPointList = localRouteList[k]._wayPointList;	
			 
	listHTML += "<tr> <td onclick=\"listRowTapped("+k+")\" class=\"full-width mdl-data-table__cell--non-numeric\">" + localRouteName + "  (" + localRouteStartDate + ")<div class=\"subtitle\">" + localRouteSourcePort + " &rarr; " + localRouteDestinationPort +"</div>" + "<div class=\"subtitle\">" + "Ship: " + localRouteShip + " ,Distance: "+  localRouteDistance + ", Stops: " + localRouteWayPointList.length + ", Cost: "+ localRouteCost +"</div></td></tr>";
		};
		
	}
	else
	{
		window.alert("No routes to display,Please create a route");
	};
	
	if(viewRouteTableRef != null)
	{
		viewRouteTableRef.innerHTML += listHTML;
	}

//Gives which row of the table is clciked on and is redirected to viewRoute.html
function listRowTapped(routeIndex)
{
	console.log(localRouteList[routeIndex]);
	sessionStorage.setItem("selectedRouteIndex", JSON.stringify(routeIndex));
	window.location.href = "viewRoute.html";

};
	
	
	
