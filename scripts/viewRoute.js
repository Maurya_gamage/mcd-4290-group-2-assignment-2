//Displays the route details of localRouteList at Position (routeIndex)
window.onload = function() {
	var routeIndex = JSON.parse(sessionStorage.getItem("selectedRouteIndex"));
	var selectedRoute = localRouteList[routeIndex];

if(selectedRoute == null )
{ 
		window.alert("Route has not been selected ,You will be redirected to main page");
		window.location.href = "index.html";
}
else 
{
	

	var routeSource = (selectedRoute._sourcePort);
	var routeDest = (selectedRoute._destinationPort);
	var routeWaypoints = (selectedRoute._wayPointList);
	console.log(routeWaypoints)
	console.log(routeWaypoints[0])
	var points = [];

	var routeSourceCoordinates = [routeSource[0].lng,routeSource[0].lat];
	var routeDestCoordinates = [routeDest[0].lng,routeDest[0].lat];
	points.push(routeSourceCoordinates);
	points.push(routeDestCoordinates);
	 //console.log([routeSourceCoordinates,waypoints,routeDestCoordinates])
	 console.log((points))


	mapboxgl.accessToken = 'pk.eyJ1IjoidmluZXRoIiwiYSI6ImNrZTVrODRmdjEzdHUzMm43eG00czRlZWgifQ.gK2Agmj8cAGRYVHEGab6VA';
	var map = new mapboxgl.Map({
	container: 'map',
	style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
	center: (routeSourceCoordinates), // starting position [lng, lat]
	zoom: 3 // starting zoom
	});
		map.on('load', function() {
				var marker = new mapboxgl.Marker({color: 'green'})
				.setLngLat(routeSourceCoordinates)
				.setPopup(new mapboxgl.Popup().setHTML(routeSource[0].country + ", " + routeSource[0].name))
				.addTo(map);
				 marker.togglePopup();
					
					var marker = new mapboxgl.Marker({color: 'red'})
				.setLngLat(routeDestCoordinates)
				.setPopup(new mapboxgl.Popup().setHTML(routeDest[0].country  + ", " + routeDest[0].name))
				.addTo(map);
				 marker.togglePopup();
				 
				 for(let x=0; x<routeWaypoints.length; x++)
				 {
					points.splice(-1,0,routeWaypoints[x])
					var marker = new mapboxgl.Marker({color: 'orange'})
					.setLngLat(routeWaypoints[x])
					.setPopup(new mapboxgl.Popup().setHTML("Waypoint: " + (x+1)))
					.addTo(map);
					 marker.togglePopup();
				 }
	console.log((points))
				map.addSource('route', {
					'type': 'geojson',
					'data': {
					'type': 'Feature',
					'properties': {},
					'geometry': {
						'type': 'LineString',
						'coordinates': (points)
						}	
					}	
				});
				console.log((points))
				map.addLayer({
					'id': 'route',
					'type': 'line',
					'source': 'route',
					'layout': {
						'line-join': 'round',
						'line-cap': 'round'
					},
					'paint': {
						'line-color': '#888',
						'line-width': 4
					}
				}); 
		});
		
		document.getElementById("distanceOutput").innerHTML = (Number(selectedRoute._distance).toFixed(2) + " Km")
		document.getElementById("estimatedTimeOutput").innerHTML = (Number(selectedRoute._time).toFixed(2) + " Hours");
		document.getElementById("costOutput").innerHTML = ( "$" + Number(selectedRoute._cost).toFixed(2));
		document.getElementById("routeInfo").innerHTML = ("Ship: " + selectedRoute._ship + "</br>" + "Departure Date: " + selectedRoute._startDate);



	let removeButton = document.getElementById("removeRoute");
    removeButton.addEventListener('click', function(e){
		localRouteList.splice(routeIndex,1);
		console.log(localRouteList)
		if (typeof(Storage) !== "undefined")
    {
        
		//Stringify  to a JSON string
		let jsonportlist = JSON.stringify(localRouteList);
		
        //store this JSON string to local storage using the key 
		localStorage.setItem("routeList",jsonportlist);
		
		window.alert("Route Deleted successfully");
		window.location.href = "index.html";

    }
	});
	
	
	
	let postponeButton = document.getElementById("postpone");
	//deletes old route and push new route with updated departure date
    postponeButton.addEventListener('click', function(e){
		
		var newDateRef = document.getElementById("newStartDate").value;
		console.log(selectedRoute);
		selectedRoute._startDate = newDateRef;
		console.log(selectedRoute);

		localRouteList.splice(routeIndex,1);
		console.log(localRouteList)
		localRouteList.push(selectedRoute);
		
		//Loops throught local routes and sorts them according to earliest date first
		if (localRouteList.length>=1)
		{
			for(var j =0; (j+1)<localRouteList.length; j++)
			{
				console.log(localRouteList);
				console.log(j);
				localRouteList.sort(function() 
				{	
				console.log(localRouteList.length
				var dateA = new Date(localRouteList[j]._startDate), dateB = new Date(localRouteList[j+1]._startDate);
				return dateB - dateA;
				});
				console.log(localRouteList);
			};
		};
		if (typeof(Storage) !== "undefined")
    {
        
		//Stringify  to a JSON string
		let jsonportlist = JSON.stringify(localRouteList);
		
        //store this JSON string to local storage using the key 
		localStorage.setItem("routeList",jsonportlist);
		
		window.alert("Route Departure date updated successfully");
		window.location.href = "index.html";

    } 
	});
}

}









//postponeRoute