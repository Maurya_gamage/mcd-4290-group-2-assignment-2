// Function for Gathering details of the Port
function createNewPort()
	{
		//Gets port details entered by user to input boxes to be saved to local storage

		portNameRef = document.getElementById("portName").value;	// Gives the name of the Port in the HTML file
		console.log(portNameRef);									
		countryRef = document.getElementById("country").value;		// Shows the country of the Port in the HTML file
		typeRef = document.getElementById("type").value;			// Gives the type of the Port to the HTML file
		sizeRef = document.getElementById("size").value;            // Gives the size of the Port in the HTML file                                                                                          
		locationRef = document.getElementById("location").value;
		
		var locationCountry = locationRef + ", " + countryRef;
		//Uses opencage API to forward gecode Country to get coordinates
		var data = {
			 q: locationCountry,
			 key: "ea998a4a9c0f428299766af2959064ee",
			 limit: 1,
			 callback: "locationresponse"
			};
			
			jsonpRequest("https://api.opencagedata.com/geocode/v1/json", data);
			function jsonpRequest(url, data)
			{
				// Build URL parameters from data object.
				 var params = "";
				 // For each key in data object...
				 for (var key in data)
				 {
					 if (data.hasOwnProperty(key))
					 {
						if (params.length == 0)
					 {
						// First parameter starts with '?'
						params += "?";
					 }
						else
					 {
						 // Subsequent parameter separated by '&'
						 params += "&";
					 }
					 var encodedKey = encodeURIComponent(key);
					 var encodedValue = encodeURIComponent(data[key]);
					 params += encodedKey + "=" +encodedValue;

					 }
				 }
				 var script = document.createElement('script');
				 script.src = url + params;
				 document.body.appendChild(script);
			};
       
	};
//takes location coordinates as input parameters from opencage Api 	
function locationresponse(locArray)
{
	if(locArray.results.length>0)
	{
		console.log(locArray);
		latRef = locArray.results[0].geometry.lat;
		

		lngRef = locArray.results[0].geometry.lng;
		console.log(lngRef);
		 let portKey = "portlist";
			
			var portListInstance = new PortList();
			
			portListInstance.addPort();
		if (typeof(Storage) !== "undefined")
		{
			//Stringify deckInstance to a JSON string
			var jsonportlist = JSON.stringify(localPortList);
			
			//store this JSON string to local storage using the key 
			localStorage.setItem(portKey,jsonportlist);
			
			window.alert("Route added successfully");
			//window.location.href = "index.html";

		}
		else
		{
			window.alert("Error: localStorage is not supported by current browser.");
		};
	}
	else 
	{
		window.alert("No results Found. Verify and enter port location");
	};
};