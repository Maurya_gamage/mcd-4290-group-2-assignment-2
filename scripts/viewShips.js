// Showing all the ships in the form of a table 
var viewShipTableRef = document.getElementById('viewShipTable');
var shipList = [];

var data = {
 callback: "shipsResponse"
};

jsonpRequest("https://eng1003.monash/api/v1/ships/", data);
function jsonpRequest(url, data)
{
	// Build URL parameters from data object.
	 var params = "";
	 // For each key in data object...
	 for (let key in data)
	 {
		 if (data.hasOwnProperty(key))
		 {
			if (params.length == 0)
		 {
			// First parameter starts with '?'
			params += "?";
		 }
			else
		 {
			 // Subsequent parameter separated by '&'
			 params += "&";
		 }
		 var encodedKey = encodeURIComponent(key);
		 var encodedValue = encodeURIComponent(data[key]);
		 params += encodedKey + "=" +encodedValue;

		 }
	 }
	 var script = document.createElement('script');
	 script.src = url + params;
	 document.body.appendChild(script);
}

//Creates row elements for each ship with relavent details and is added to html table for all API ships
function shipsResponse(shipArray)
{
	shipList = shipArray;
 
    let listHTML = "";
	
		for (let i=0; i<shipList.ships.length; i++)
			{
				let shipName = shipList.ships[i].name;
				let shipSpeed = shipList.ships[i].maxSpeed;
				let shipRange = shipList.ships[i].range;
				let shipDescription = shipList.ships[i].desc;
				let shipCost = shipList.ships[i].cost;
				let ShipStatus = shipList.ships[i].status;
				let shipComments = shipList.ships[i].comments +"-(API)";
			
			
    // And sample JavaScript code that would generate the HTML above is:
    //
	listHTML += "<tr> <td class=\"full-width mdl-data-table__cell--non-numeric\">" + shipName + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + shipSpeed + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + shipRange + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + shipDescription + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + shipCost + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + ShipStatus + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + shipComments + "</td></tr>";
			};

    // Insert the list view elements into the flights list.
	viewShipTableRef.innerHTML += listHTML;
			
};

//Creates row elements for each ship with relavent details and is added to html table for all user created ships

let listHTML = "";
for (let i=0; i<localShipList.length; i++)
	{
		
				let localshipName = localShipList[i].name;					// Name for the Ships 
				let localshipSpeed = localShipList[i].maxSpeed;			// Speed for the Ships
				let localshipRange = localShipList[i].range;				// Range for the Ships 
				let localshipDescription = localShipList[i].desc;	// Description of the Ships 
				let localshipCost = localShipList[i].cost;					// Cost for the Ships 
				let localShipStatus = localShipList[i].status;				// Status of the Ships 
				let localshipComments ="(User Created)";		
	
	listHTML += "<tr> <td class=\"full-width mdl-data-table__cell--non-numeric\">" + localshipName + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + localshipSpeed + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + localshipRange + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + localshipDescription + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + localshipCost + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + localShipStatus + "</td>"
			 + 	"<td class=\"full-width mdl-data-table__cell--non-numeric\">" + localshipComments + "</td></tr>";
	};
	viewShipTableRef.innerHTML += listHTML;
			
